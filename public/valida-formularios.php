<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
    <title>Formulário de inscrição</title>
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href='https://fonts.googleapis.com/css?family=Rock+Salt' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700,100' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:300,700,900,500' rel='stylesheet' type='text/css'>

    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="assets/css/animate.css" rel="stylesheet">
    <link href="assets/css/plugins.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">

</head>

<body>
    
    <?php 
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            $nome = $_POST['nome'];
            $nascimento = $_POST['nascimento'];
            $email = $_POST['email'];
            $responsavel = $_POST['responsavel'];
            $mensagem = $_POST['mensagem'];
            $foto_path = $_POST['foto_path'];

            if ($nome == "") {
                $erro_nome = '* O nome é obrigatório';
            }
        } 
    ?>
    
    <div class="container bg-success">
        <div class="col-md-12">
            <form name="" method="post" action="valida-formularios.php">
                <!-- caixas de texto -->
                <div class="form-group" style="margin-top: 30px;">
                    <label for="usuario">Nome completo</label>
                    <input class="form-control form-control-sm" type="text" id="" name="nome" placeholder="Digite seu nome completo">
                    <small class="text-danger" ><?php echo $erro_nome;?></small>
                </div>

                <div class="form-group">
                    <label for="usuario">Data de Nascimento</label>
                    <input class="form-control form-control-sm" type="date" id="nascimento" name="nascimento" placeholder="Digite sua data de nascimento">
                    <div class="text-danger" >coloque uma data</div>
                </div>

                <div form-group>
                    <label for="email">Email</label>
                    <input class="form-control form-control-sm" type="email" id="email" name="email" placeholder="Digite seu email">
                    <div class="text-danger" >email inválido</div>
                </div>

                <div form-group>
                    <label for="email">Responsável</label>
                    <input class="form-control form-control-sm" type="text" id="responsavel" name="responsavel" placeholder="Digite o nome e o telefone do responsável">
                    <div class="text-danger" >coloque o nome  e o telefone do responsável</div>
                </div>

                <div form-group>
                    <label for="email">Tipo sanguíneo</label>
                    <input class="form-control" type="text" id="tiposang" name="tiposang" placeholder="Digite seu tipo sanguíneo">

                </div>

                <!-- select -->

                <!-- textarea -->
                <div class="form-group">
                    <label for="mensagem">Fale mais sobre você</label>
                    <textarea class="form-control" id="mensagem" name="mensagem" rows="3"></textarea>
                    <div class="text-danger" >Fale pelo menos 1 linha sobre você</div>
                </div>

                <!-- file input -->
                <div class="form-group">
                    <label for="foto">Selecione uma foto</label>
                    <input class="form-control-file" type="file" id="foto" name="foto_path">
                    <div class="text-danger" >Envie uma foto sua</div>
                </div>

                <!-- button -->
                <input type="hidden" value="1" name="tbl">
                <input class="btn btn-success btn-block btn-lg mb60" type="submit" name="" value="Enviar">
                <div class="text-primary" >Informações de inscrição enviadas, garanta sua vaga efetuando o pagamento</div>

            </form>

        </div>

    </div>

</body>



<pre> <?php print_r($_POST); ?> </pre>