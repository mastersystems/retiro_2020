<!DOCTYPE html>

<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="width=device-width, initial-scale=1" name="viewport">
    <meta content="" name="description">
    <meta content="" name="author">
    <link rel="short cut icon" type="image/png" href="assets/images/adventista do setimo dia.jpg">
    <title>Formulário de inscrição</title>
    <link href="assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link href='https://fonts.googleapis.com/css?family=Rock+Salt' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,300,700,100' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Raleway:300,700,900,500' rel='stylesheet' type='text/css'>

    <link href="assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <link href="assets/css/animate.css" rel="stylesheet">
    <link href="assets/css/plugins.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">

    <script type="text/javascript">
    
    function validaForm(){
        d = document.forminscritos;
           
	    if (d.nome.value == ""){
		    alert("O Nome deve ser informada!");
		    d.nome.focus();
		    return false;	
	    }

        if ((d.nascimento.value == "")||(d.nascimento.value == 0)){
		    alert("A Data de Nascimento deve ser informada!");
		    d.nascimento.focus();
		    return false;	
	    }
	
	    if (d.email.value == ""){
		    alert("O E-mail deve ser informada!");
		    d.email.focus();
		    return false;	
	    }
	
	    if (d.responsavel.value == ""){
		    alert("O Responsavel deve ser informado!");
		    d.valor.focus();
		    return false;	
	    }
 
        if (d.tiposang.value == ""){
		    alert("O Tipo Sanguineo deve ser informado!");
		    d.tiposang.focus();
		    return false;	
	    }

    }
    </script>

</head>

<body>
    <?php 
        $acao = "Salvar"; 
    ?>

    <div class="container bg-success">
        <div class="col-md-12">
            <form name="forminscritos" method="post" action="../middleware/grava_dados.php" >
                <!-- caixas de texto -->
                <div class="form-group" style="margin-top: 30px;">
                    <label >Nome completo</label>
                    <input class="form-control form-control-sm" type="text" id="nome" name="nome" required data-validation-required-message="Digite seu nome completo">
                </div>

                <div class="form-group">
                    <label >Data de Nascimento</label>
                    <input class="form-control form-control-sm" type="date" id="nascimento" name="nascimento" required data-validation-required-message="Digite sua data de nascimento">
                </div>

                <div form-group>
                    <label for="email">Email</label>
                    <input class="form-control form-control-sm" type="email" id="email" name="email" required data-validation-required-message="Digite seu email">
                    <small class="form-text">Email será validado</small>
                </div>

                <div form-group>
                    <label for="responsavel">Responsável</label>
                    <input class="form-control form-control-sm" type="text" id="responsavel" name="responsavel" placeholder="Por favor informe seu responsável se for menos de 18 anos.">
                    <small class="form-text">Coloque o nome e o telefone</small>
                </div>

                <div form-group>
                    <label for="tiposang">Tipo sanguíneo</label>
                    <input class="form-control" type="text" id="tiposang" name="tiposang" placeholder="Não é obrigatório mas para casos de urgência">

                </div>



                <!-- select -->

                <!-- textarea -->
                <div class="form-group">
                    <label for="mensagem">Fale mais sobre você</label>
                    <textarea class="form-control" id="mensagem" name="mensagem" rows="3" placeholder="Digite algo que revele um pouco sobre você." ></textarea>
                </div>

                <!-- file input -->
                <!--<div class="form-group">
                    <label for="foto">Selecione uma foto</label>
                    <input class="form-control-file" type="file" id="foto" name="foto_path">
                </div>-->

                <!-- button -->
                <input type="hidden" value="1" name="tbl">
                <input class="btn btn-success btn-block btn-lg mb60" type="submit" name="Submit" value="<?php echo $acao;?>">

            </form>

        </div>

    </div>

</body>