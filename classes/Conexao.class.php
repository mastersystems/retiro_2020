<?php
/*
|-------------------------------------------------------------------------
|	CLASSE PARA CONEXÃO COM BANCO DE DADOS
|-------------------------------------------------------------------------
*/
class Connection
{
// Dados do seu banco de dados
const HOST	= 'localhost';//'jmtservico-2.mysql.uhserver.com'; //"127.0.0.1"; //$host = '200.98.197.211', //jmtservico-2.mysql.uhserver.com
const USER	= "root"; //"root"; //$usuario = 'jmtservico_1', //jmtservico_2
const PW	= ''; //"mysql";	//$senha = 'refrigera1'; //r3fr1g3r@2
const DATABASE  = "retiro_2020";
// Dados da classe.
public $query;
public $mysqli;
public $result_array = array();
public $result_assoc = array();
// metodo construtor
function Connection()
{
	$this->mysqli = new mysqli(self::HOST, self::USER, self::PW, self::DATABASE );
	if (mysqli_connect_errno()) {
		printf("Falha na conexão com o banco de Dados.", mysqli_connect_error()); 
	}
}
function query_sql($query)
{
	// funcao que executa a query.
	return $this->query =  $this->mysqli->query($query);

}
function result()
{
	return $this->query->fetch_row();
}
function results_array()
{
	// Pego todos os resultados e jogo em uma array
	while($dados = $this->query->fetch_array())
	{
		$this->result_array[] = $dados;
	}
	// retorno a array.
	return $this->result_array;
}
function results_assoc()
{
	// Pego todos os resultados e jogo em uma array
	while($dados = $this->query->fetch_assoc())
	{
		$this->result_assoc[] = $dados;
	}
	// retorno a array.
	return $this->result_assoc;
}
function rows()
{
	// um simples método para contar as linhas
	return $this->query->num_rows;
}
public function escapeString( $string )
    {
        return $this->mysqli->real_escape_string($string);
    }
public function insertion_id()
    {
        return $this->mysqli->insert_id;
    }
}
?>
