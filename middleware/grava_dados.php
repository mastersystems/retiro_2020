<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<head>
<?php
include "../classes/Inscritos.class.php";
include "../classes/Contatos.class.php";
?>
  
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
</head>
<body>
<?php
$acao       = $_POST["Submit"]; // define a acao
$tbl        = $_POST["tbl"]; // define qual a tabela que recebera a acao
if(isset($_POST["id"])) {$id = $_POST["id"];}
$data       = date("d/m/Y");// data um dado comum
$timestamp 	= mktime(date("H")-4, date("i"), date("s"), date("m"), date("d"), date("Y"));
$data_h 	= gmdate("d/m/Y H:i", $timestamp);

if (isset($acao)) {
 	
    if ($tbl == "1")  // inscritos
    { 
        $oInscrito 		 = new Inscritos();
        $nome   		 =  $_POST["nome"];
        $responsavel   	 =  $_POST["responsavel"];
        $email   		 =  $_POST["email"];
        $data_nascimento =  $_POST["nascimento"];
        $tipo_sang   	 =  $_POST["tiposang"];
        $fale_mais   	 =  $_POST["mensagem"];
        $pago   		 =  0;
        $foto   		 =  $_POST["foto_path"];
        
        if ($acao == "Salvar") 
	    { 
            $novo_id_inscrito = $oInscrito->inserirInscrito($nome, $responsavel, $email, $data_nascimento, $tipo_sang, $fale_mais, $pago, $foto);
            if ($novo_id_inscrito != null) {
                echo ("<script>alert('Operacao realizada com sucesso');</script>");
            }		
		    
	    }
	    
        
        echo "<script>location.href=\"../public/contato.html\"</script>";
    } 

    if ($tbl == "2")  // contatos
    { 
        $oContato 		 = new Contatos();
        $nome   		 =  $_POST["nome"];
        $email   		 =  $_POST["email"];
        $facebook           =  $_POST["facebook"];
        $comentario   	 =  $_POST["comentario"];
        
        if ($acao == "Salvar") 
	    { 
		    $novo_id_contato = $oContato->inserirContato($nome, $email, $facebook, $comentario);		
		    
	    }
	    
        echo ("<script>alert('Operação realizada com sucesso.');</script>");
        echo "<script>location.href=\"../public/contato.html\"</script>";
    } 

}
//fim 
?>
</body>


</html>
